---
title: "RKafka"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## RKafka
This is an R Markdwon document made to outline the process for connecting R to Kafka streams. Additional documentation is here: https://www.r-bloggers.com/building-a-kafka-and-spark-streaming-pipeline-part-i/  


Also, we will need to install some libraries, troubleshoot the java and r problems that arise, and go through all the documentation provided online. 

### Step 1: Install Java
Once Java has been installed, proceed to step 2.


### Step 2: Install Zookeeper
Zookeeper can be download from this `{r} [link](http://zookeeper.apache.org/releases.html)` 

CD to the directory where you unpacked the zookeeper tar ball. Then make a data directory via *mkdir data* in terminal.  

Edit the zoo.cfg file, this will be automatically created if it doesn't already exist using vim conf/zoo.cfg in terminal

```{r}
library(rkafka)
library(rJava)
library(rkafkajars)


```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
